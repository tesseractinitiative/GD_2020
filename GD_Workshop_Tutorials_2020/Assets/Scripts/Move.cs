﻿/*
----------------------------------------------------------------------
Originally written by something999
Part of Tesseract Initiative's Game Design Workshop (2020)
Project Link: https://gitlab.com/tesseractinitiative/GD_2020

UNITY VERSION: 2019.4.1f1
----------------------------------------------------------------------
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
---------------------------------------------------------------------- 
This script demonstrates one method of making a character
move left and right based on user inputs.

We move the character by calculating how far left or right the
character should move every frame and add that calculation
to the character's current position.
---------------------------------------------------------------------- 
*/

public class Move : MonoBehaviour
{
    /*
        While developing the game, we might modify the
        character's speed or add code granting the character
        more complex movements.
        
        For convenience, we'll store the speed in a public variable,
        making it accessible from Unity's Inspector.
    */
    public float speed = 5f; // How fast the character moves. 
    public static bool isPaused = false; // If the character has been paused for throw mode
    
    /*
        Unity calls Update once per frame. This makes it an ideal 
        spot for defining actions that are frame-dependent, such as 
        tracking user input. 
        
        Input: User inputs (left and right arrow keys, 'A', and 'D')
        Output: None
    */
    private void Update()
    {
        // Check if the character has been paused for throw mode
        if (isPaused)
        {
            return;
        }

        float direction = Input.GetAxis("Horizontal"); // Track if the character is moving left or right
        
        /*
            We need Time.deltaTime to let the character move at 
            x units per second. Without it, our character would move 
            x units per frame.
        */
        float distance = direction * speed * Time.deltaTime;
        
        /*
            After calculating, add a vector representing how far the character
            needs to move to the character's current position.
        */
        transform.position = transform.position + new Vector3(distance, 0, 0); 
    }
}
