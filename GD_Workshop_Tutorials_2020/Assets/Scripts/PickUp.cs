﻿/*
----------------------------------------------------------------------
Written by something 999 and Lynn D.
Part of Tesseract Initiative's Game Design Workshop (2020)
Project Link: https://gitlab.com/tesseractinitiative/GD_2020

UNITY VERSION: 2019.4.1f1
----------------------------------------------------------------------
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
---------------------------------------------------------------------- 
This script demonstrates one method of making a character
pick up and throw an object.

We will be attaching this script to the object.

Our Process:
(Picking up objects)
1. Determine whether the character is within pickup distance of the object
(we will call this the threshhold).
2. Wait for the character's input (we'll determine what key that needs to be).
3. Attach the object to the player.
4. Record that the player has picked up the object (so the player can't repeat
this until the object has been put down).

(Throwing objects)
1. Wait for the character's input to activate throw mode.
2. Allow the player to determine the direction the object will go.
3. Wait for character's input to throw the object.
4. Record that the player no longer has the object.

To help our player, we will also display a notification whenever the 
player is within pickup distance of the object. This notification informs
the user what key to press.
---------------------------------------------------------------------- 
*/
public class PickUp : MonoBehaviour
{
    /*
        While developing the game, we might modify the
        attributes or add code granting the character more 
        complex pickup behaviors.
        
        For convenience, we'll store the variables in a public variable,
        making them accessible from Unity's Inspector.
    */
    public string key = ""; // Key user needs to press to pickup object. Max limit: 1 character
    public float threshold = 2f; // The max distance object shall be from the character 
    public GameObject player = null; // The character who will pickup the object 
    public GameObject notification = null; // The notification that appears above the object
    public bool hasBeenPickedUp = false; // Has the object been picked up?
    public float throwStrength = 4f; // Throwing strength
    public float throwAngle = 0f; // The angle in degrees the object will be thrown
  
    /*
        In order to update the notification to display
        the key used to pickup the object, we need a reference
        to the notification's text.
        
        Note: If the notification object does not have a TextMesh
        component, it will be null. For the rest of the script to work,
        this reference can't be null.
    */
    
    /*
        We will also have variables that will record whether the player wants 
        to throw the object and the direction the player will throw the object.
        We won't need to edit these variables, so we'll make them private 
        (hidden in Unity's Inspector).
    */
    private bool throwMode = false; // Throw mode pauses the player movements for throwing
    private float direction = 0; // The direction the object will be thrown | -1: left, 0: up, 1: right

    
    public SpriteRenderer sprite = null; // We use this component to calculate how high the object needs to be above the character when the object is picked up.
    private void Start()
    {
        notification.GetComponent<TextMesh>().text = key; // Cannot be null
    }
    
    /*
        Unity calls Update once per frame. This makes it an ideal 
        spot for defining actions that are frame-dependent, such as 
        tracking user input. 
        
        Input: User input (the string specified by key)
        Output: None
    */
    private void Update()
    {
        // First: Check if the object has not been picked up 
        if (hasBeenPickedUp == false)
        {
            // Second: Check is the character is within pickup distance 
            if (CheckDistance())
            {
                // If in range, show the notification.
                ShowNotification(true);
                // Check user input
                if (Input.GetKeyDown(key))
                {
                    // Get the object
                    GetObject();
                    // Record that the object has been picked up.
                    hasBeenPickedUp = true;
                }
            }
            else
            {
                // If not in range, don't display the notification.
                ShowNotification(false);
            }
        }
        else
        {
            // If the object has been picked up, set up for throwing

            // First: Check the user input to start throw mode.
            if (Input.GetKeyDown(key) && !throwMode)
            {
                // Record that throw mode has started
                throwMode = true;
                // Pause the player so they can't move
                Move.isPaused = throwMode;
                // Indicate the default direction of the throw to be upward
                notification.GetComponent<TextMesh>().text = "^";
            }
            else if (throwMode)
            {
                // If throw mode has started

                // Second: Allow the player to indicate the direction the object will be thrown
                CheckDirection();
                
                // Third: Check for user input again to throw the object
                if (Input.GetKeyDown(key))
                {
                    // Throw the object
                    ThrowObject(direction);
                    // Record that the object has been thrown and throw mode ended
                    hasBeenPickedUp = false;
                    throwMode = false;
                    // Allow the player to move again
                    Move.isPaused = false;
                    // Reset the direction to 0 (up)
                    direction = 0;
                    // Reset the notification to be the pick up key
                    notification.GetComponent<TextMesh>().text = key;
                }
            }
           
        }
    }
    
    /*
        Measure the distance between the object and player
        to see if the player is within pickup distance of
        the object.
        
        Input: None
        Output: Bool (true / false)
    */
    private bool CheckDistance()
    {
        // Calculate the horizontal distance between the objects
        float distance = Mathf.Abs(player.transform.position.x - this.transform.position.x);
        
        // Check if the distance is less than or equal to the threshold
        if (distance <= threshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /*
        Attach the object to the player.
        We do this by making an object a child of our character
        (the object will automatically follow the character,
        saving us some effort).
        
        Input: None
        Output: None
    */
    private void GetObject()
    {
        // Set the object to stop moving
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        // Move the object directly above the character
        this.transform.position = player.transform.position + new Vector3(0, sprite.bounds.extents.y, 0);
        // Set the object to be the character's child
        this.transform.parent = player.transform;
    }

    /*
        Check the direction the player wants to throw.
        We do this by checking which arrow key was last pressed
        and set the direction accordingly. We also indicate to the
        user what direction the object will thrown.

        Input: None
        Output: None
    */
    private void CheckDirection()
    {
        // If the player wants to throw upward
        // Mapped controls: Up arrow key, W key
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            direction = 0;
            notification.GetComponent<TextMesh>().text = "^";
        }

        // If the player wants to throw to the left
        // Mapped controls: Left arrow key, A key
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            notification.GetComponent<TextMesh>().text = "< " + key;
            direction = -1;
        }

        // If the player wants to throw to the right
        // Mapped controls: right arrow key, D key 
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            direction = 1;
            notification.GetComponent<TextMesh>().text = key + " >";
        }
    }


    /*
        Throw the object in the specified direction.

        Input: float
        Output: None
    */
    private void ThrowObject(float direction)
    {
        // Stop showing the notification once the object has been thrown
        ShowNotification(false);

        //Calculate the angle in terms of pi
        float piAngle = throwAngle * Mathf.PI / 180;
        // Calculate the x and y direction of the velocity vector
        float xDirection = Mathf.Cos(piAngle) * direction;
        float yDirection = Mathf.Sin(piAngle);
        // Calculate the velocity of the object as it is thrown
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(xDirection, yDirection) * throwStrength;
        // Detach the object from the player
        this.transform.parent = null;

    }
    
    /*
        Determine whether to display the notification in the game.
        
        Input: Bool (Are we displaying the notification or not?)
        Output: None
    */
    private void ShowNotification(bool show)
    {
        // Check that the notification reference is not null.
        if (notification != null)
        {
            notification.SetActive(show);
        }
    }
}