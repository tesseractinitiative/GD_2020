﻿/*
----------------------------------------------------------------------
Originally written by something999
Part of Tesseract Initiative's Game Design Workshop (2020)
Project Link: https://gitlab.com/tesseractinitiative/GD_2020

UNITY VERSION: 2019.4.1f1
----------------------------------------------------------------------
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
---------------------------------------------------------------------- 
This script shows one method of making a character jump
after receiving user input.
    
Characters or objects with this script MUST have a Rigidbody2D component.

Our jump consists of 2 vertical forces. 
Force 1 represents how high our character can jump.
Force 2 represents how long our character can stay in the air
(by dragging our character to the ground).
---------------------------------------------------------------------- 
*/

public class Jump : MonoBehaviour
{
    /*
        While developing the game, we might want to modify the
        forces or add code to this script that grants the character
        more complex movements.
            
        For convenience, we'll store each force in public variables,
        making them accessible from Unity's Inspector.
    */
 
    public float upForce = 5f;
    public float downForce = 1f;
    
    /*
        To allow Unity's physics engine to act upon
        our object, we need to store a reference to the character's
        Rigidbody2D component. 
        
        In order to assign a Rigidbody2D component to the script, we'll
        make the reference public.
        
        Note 1: An exception will be thrown if this variable is Null.
        Note 2: Rigidbody2D affects 2D objects. Rigidbody affects 3D objects.
        As Rigidbody is a special term in Unity, it wouldn't be a good idea 
        to also name our variable "rigidbody."
    */
    
    public Rigidbody2D rb = null; // This CANNOT be Null.
    
    /*
        In our proposed implementation, downForce
        represents gravity, so we set our character's gravity to this force. 
    */ 
    private void Start()
    {
        rb.gravityScale = downForce;
    }
    
    /*
        Unity calls Update once per frame. This makes it an ideal 
        spot for defining actions that are frame-dependent, such as 
        tracking user input.  
        
        Input: User input (spacebar)
        Output: None
    */
    private void Update()
    {
        // Check if the character has been paused for throw mode
        if (Move.isPaused)
        {
            return;
        }

        // Tracks whether the player pressed the space key.
        if (Input.GetKeyDown(KeyCode.Space))
        {
            /*
                Once the key press is detected, we apply an instant upwards 
                force to our character.
            */
            rb.AddForce(new Vector3(0, upForce, 0), ForceMode2D.Impulse);
        }
    }
}
